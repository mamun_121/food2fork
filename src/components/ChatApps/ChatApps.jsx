import React from 'react';
import './App.css';
import ChatWindow from "./ChatWindow";

class ChatApps extends React.Component{
    users = [
        { username: "Jamai" },
        { username: "Bou" }
    ];
    state = {
        messages: [

        ]
    };

    postMessages = msg => {
        this.setState(prevState => ({
            messages: [...prevState.messages, msg]
        }))
    };


    render() {
        const { messages } = this.state;
        return (
            <div className="App">
                <main className="App-main">
                    <div className="container">
                        {
                            this.users.map(user => (
                                <ChatWindow
                                    key={user.username}
                                    username={user.username}
                                    messages={messages}
                                    onPostMessage={this.postMessages}
                                />
                            ))
                        }
                    </div>
                </main>
            </div>
        );
    }
}

export default ChatApps;