import React from 'react';
import MessageHistory from "./MessageHistory";
import MessageInput from "./MessageInput";
import PropTypes from "prop-types";

class ChatWindow extends React.Component{

    composeMessage = msg => {
        const {username} = this.props;
        const text = msg;
        // object shape should be same, key is username and text, see ChatApp -> state -> messages
        const msgObject = {
            username,
            text
        };

        this.props.onPostMessage(msgObject);
    };
    render() {
        const { username, messages } = this.props;
        return(
            <div className="chat-window">
                <h2>Super Awesome Chat</h2>
                <div className= {username === "Jamai" ? 'message sender' : 'message recipient'}>
                    <h1> {username} </h1>
                </div>
                <MessageHistory
                    messages = {messages} // only show the initial msg
                    username = {username}
                />

                <MessageInput
                    onPostMessage={this.composeMessage} // show all the meg + typed by the users
                />
            </div>
        );
    }
}
ChatWindow.propTypes = {
  username: PropTypes.string.isRequired,
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            username: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
        })
    ),
    onPostMessage: PropTypes.func.isRequired,

};
export default ChatWindow;