import React from 'react';

class MessageInput extends React.Component{
    state = {
        value: ""
    };

    isDisabled = () => {
        return this.state.value === "";
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
    };
    handleSubmit = e => {
        e.preventDefault();
        // this.props.onAddItem(e.target.value); // Nope! target is button
        this.props.onPostMessage(this.state.value);
        this.setState({
            value: '',
        })
    };


    render() {
        return(
            <form onSubmit={this.handleSubmit} className="input-group">
                <input
                    type="text"
                    placeholder="Enter Your Massages..."
                    value={this.state.value}
                    onChange={this.handleChange}
                />
                <div className="input-group-append">
                    <button className="btn submit-button" disabled={this.isDisabled()}>
                        SEND
                    </button>
                </div>
            </form>
        );
    }
}

export default MessageInput;