import React from 'react';
import RecipeList from "../RecipeList";
import Search from "../Search";

import {recipeData} from '../data/tempList';

class Recipes extends React.Component{

    state = {
        recipes: recipeData,
        search: '',
    };

    handleChange = (event) => {
        this.setState({search: event.target.value});
    };

    handleSubmit = (event) => {
        console.log(this.state.search);
        this.setState({
           search: '',
        });
        event.preventDefault();
    };

    render() {
        const {search, recipes} = this.state;

        return(
            <React.Fragment>
                <Search
                    search={search}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                />
                <RecipeList
                    recipes={recipes}
                />
            </React.Fragment>
        );
    }
}

export default Recipes;