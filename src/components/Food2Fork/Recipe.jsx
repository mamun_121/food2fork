import React from 'react';
import {Link} from "react-router-dom";

class Recipe extends React.Component{
    render() {
        const {publisher, title, source_url, recipe_id, image_url} = this.props.recipe;
        return(
            <div className='col-10 mx-auto col-md-6 col-lg-4 my-3'>
                <div className='card' style={{height : '100%'}}>
                    <img className="card-img-top" src={image_url} alt="Card" />
                    <div className="card-body">
                        <h5 className="card-title text-capitalize">{title}</h5>
                        <h6 className="text-warning text-slanted">{`provided by ${publisher}`}</h6>
                    </div>
                    <div className='card-footer'>
                        <Link to={`/recipes/${recipe_id}`}
                              className = 'btn btn-primary text-capitalize mr-2'
                        >
                            details
                        </Link>
                        <a href={source_url} target='_blank' rel="noopener noreferrer"
                           className='text-capitalize btn btn-success' >
                            recipe url
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Recipe;