import React from 'react';

class Search extends React.Component{
    render() {
        const {search, handleChange, handleSubmit} = this.props;
        return(
            <div className='container'>
                <div className='row'>
                    <div className='col-10 mx-auto col-md-8 text-center'>
                        <h1 className='text-slanted text-capitalize'>
                            search recipes with{' '}<strong className='text-orange'>Food2Fork </strong>
                        </h1>
                        <form onSubmit={handleSubmit} className='mt-4'>
                            <label htmlFor='search' className='text-capitalize'>
                                types recipes separated by comma
                            </label>
                            <div className='input-group'>
                                <input
                                    type='text'
                                    name='search'
                                    className='form-control'
                                    placeholder='chicken, onion, carrots'
                                    value={search}
                                    onChange={handleChange}
                                />
                                <div className="input-group-append">
                                    <button
                                        className="input-group-text bg-primary text-white"
                                        type='submit'
                                    >
                                        <i className='fas fa-search' />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Search;