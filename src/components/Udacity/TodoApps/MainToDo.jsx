import React, { Component } from "react";
import './MainApp.css';
import AddItem from "./AddItem";
import DeleteItem from "./DeleteItem";
import ItemList from './ItemList';

class MainToDo extends Component {
    state = {
        items: []
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
    };

    addItem = item => {
        this.setState(oldState => ({
            items: [...oldState.items, item],
        }));
    };

    deleteLastItem = event => {
        this.setState(prevState => ({ items: this.state.items.slice(0, -1) }));
    };

    inputIsEmpty = () => {
        return this.state.value === "";
    };

    noItemsFound = () => {
        return this.state.items.length === 0;
    };


    render() {
        return(
            <div className="App">
                <main className="App-main">
                    <h2>Shopping List</h2>

                    <AddItem
                        onAddItem={this.addItem}
                    />
                    <DeleteItem
                        onDeleteLastItem={this.deleteLastItem}
                        onNoItemsFound={this.noItemsFound()}
                    />


                    <ItemList items={this.state.items} />
                </main>
            </div>
        );
    }
}

export default MainToDo;