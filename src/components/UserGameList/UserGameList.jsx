import React from 'react';
import './App.css';
import UserInput from "./UserInput";
import UserList from "./UserList";

class UserGameList extends React.Component{
    state = {
         users: []
        // users: [
        //     { fname: "james", lname: "priest", username: "jpriest", games: 0 },
        //     { fname: "mary", lname: "jane", username: "mj4ever", games: 5 }
        // ]
    };

    // add user
    saveUser = user => {
        this.setState(prevState=> ({
           users:[...prevState.users, user]
        }))
    };

    render() {
        const {users} = this.state;
        return(
            <div className="App">
                <main className="App-main">
                    <h2>User Game List</h2>
                    <div className="container">
                        <UserInput
                            users={users}
                            saveUser={this.saveUser}
                        />
                        <UserList
                            users={users}
                        />
                    </div>
                </main>
            </div>
        );
    }
}

export default UserGameList;