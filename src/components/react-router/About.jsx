import React from 'react';

class About extends React.Component{
    render() {
        const {match} = this.props;
        console.log(this.props);
        return(
            <div>
                <h2>{match.params.topic}</h2>
            </div>
        );
    }
}

export default About;