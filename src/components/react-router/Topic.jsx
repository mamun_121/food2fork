import React from 'react';
import {Link, Route} from "react-router-dom";
import About from "./About";

class Topic extends React.Component{
    render() {
        const {match} = this.props;
        return(
            <div>
                <h1>React Router</h1>
                <p>Declarative, component based routing for React</p>
                <ul>
                    <li>
                        <Link to={`${match.url}/url-parameter`}>URL Parameters</Link>
                    </li>
                    <li>
                        <Link to={`${match.url}/pro-navigation`}>Programmatically Navigation</Link>
                    </li>
                </ul>
                <hr/>

                <Route path={`${match.path}/:topic`} component={About} />
            </div>
        );
    }
}

export default Topic;